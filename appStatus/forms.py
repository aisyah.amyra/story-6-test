from django import forms
from .models import model_form

class status_form(forms.Form):
    Status = forms.CharField(max_length = 300, label = "", widget=forms.TextInput(attrs={'placeholder': "What's my status today!:)"}))
