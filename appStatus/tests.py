from django.test import TestCase, Client, LiveServerTestCase
from .views import indexpage
from django.urls import resolve
from django.utils import timezone
from .models import model_form
from datetime import datetime
from .forms import status_form
from unittest import mock
from selenium import webdriver
import time
from selenium.webdriver.chrome.options import Options




# Create your tests here.
class Lab6UnitTest(TestCase):

    def test_lab_6_url_is_exist(self):
        response = Client().get('/mystatus/')
        self.assertEqual(response.status_code,200)

    def test_lab_6_using_index_function(self):
        found = resolve('/mystatus/')
        self.assertEqual(found.func, indexpage)

    def test_lab_6_to_do_list_template(self):
        response = Client().get('/mystatus/')
        self.assertTemplateUsed(response, 'indexpage.html')

    def test_lab_6_input_status(self):
        response = Client().post('/mystatus/' , {'Status' : 'nyoba'})
        hitung = model_form.objects.all().count()
        self.assertEqual(hitung, 1)

    def test_lab_6_model_saving_status_object_should_count(self):
        new_activity = model_form.objects.create(Date=timezone.now(), Status='waktu')
        counting_all = model_form.objects.all().count()
        self.assertEqual(counting_all, 1)

    def test_lab_6_model_time_now(self):
        now_datetime = datetime.now()
        with mock.patch("django.utils.timezone.now", mock.Mock(return_value = now_datetime)):
            new_status = model_form.objects.create(Status='nownownow')
            self.assertEqual(new_status.Date, now_datetime)

    def test_lab_6_form_charfield_more_than_300(self):
        check_form = status_form({'Status' : "kebanyakan"*1000})
        self.assertFalse(check_form.is_valid())

    
class Story6FuntionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story6FuntionalTest, self).setUp()

    def test_input_status(self):
        self.driver.get("http://localhost:8000/mystatus/")
        status_field = self.driver.find_element_by_id('id_Status')
        text_input = "Coba Coba"
        status_field.send_keys(text_input)
        time.sleep(4)
        status_field.submit()
        time.sleep(4)
        p_elements = self.driver.find_elements_by_tag_name('p')
        status_words = p_elements[-3].text.split("=")
        self.assertEqual(status_words[-1].strip(), text_input)


    