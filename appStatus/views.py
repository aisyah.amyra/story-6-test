from django.shortcuts import render, redirect 
from django.http import HttpResponse
from appStatus.forms import status_form
from appStatus.models import model_form


# Create your views here.
def indexpage(request):
    semua_status = model_form.objects.all()
    sent_data = {"all_status" : semua_status}

    if request.method == "GET":      #Tampilin form-nya
        form = status_form()
        sent_data["status_form"] = form #nambahin element ke dict
        return render (request, 'indexpage.html', sent_data)
    else: 
        isi_form = status_form(data=request.POST)     #dict yg isinya data yg telah di submit 
        if isi_form.is_valid():
            new_status = request.POST["Status"]
            # new_date = request.POST["date"]

            new_status_2 = model_form(
                Status = new_status,
                # date = new_date
            )
            new_status_2.save()
            return redirect("/mystatus/") #ke halaman hasil form
        else:
            return HttpResponse("NOT SAVED")








